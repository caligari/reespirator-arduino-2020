
#include <unity.h>
#include <reespirator/SerialMonitor/Hmi01Fsm.hpp>
#include <reespirator/Fsm/SettingsManagerFsm.hpp>
#include <reespirator/Fsm/ReespiratorFsm.hpp>
#include <reespirator/Core/Helpers/Crc16.hpp>
#include <reespirator/Fsm/VentilationFsm.hpp>
#include <reespirator/Mock/SensorsBuffer.hpp>
#include <string.h>

using namespace reespirator;

// tests
void test_crc_calculation();
void test_start_of_frame();
void test_valid_frame();
void test_bad_api_error();
void test_invalid_frame();
void test_checksum_error();
void test_resync_timeout();
void test_frame_number();
void test_machine_request();
void test_machine_params();
void test_alarm_limits();
void test_send_measures();
void test_send_cycle_data();
void test_send_alarms();
void test_multiple_frames();
void test_buffer_overflow();

// helpers
void reset_serial();
void recalculate_crc(u8* buffer);
AckErrorCode ack_error_present();

static DRE dre;

int main()
{
    // data containers
    SettingsManagerFsm set(dre);
    VentilationFsm ven(dre);
    SensorsBuffer sen(dre);
    ReespiratorFsm mac(dre);

    // tests
    UNITY_BEGIN();
    RUN_TEST(test_crc_calculation);
    RUN_TEST(test_start_of_frame);
    RUN_TEST(test_valid_frame);
    RUN_TEST(test_bad_api_error);
    RUN_TEST(test_invalid_frame);
    RUN_TEST(test_resync_timeout);
    RUN_TEST(test_checksum_error);
    RUN_TEST(test_frame_number);
    RUN_TEST(test_machine_request);
    RUN_TEST(test_machine_params);
    RUN_TEST(test_alarm_limits);
    RUN_TEST(test_send_measures);
    RUN_TEST(test_send_cycle_data);
    RUN_TEST(test_send_alarms);
    RUN_TEST(test_multiple_frames);
    RUN_TEST(test_buffer_overflow);
    UNITY_END();

    return 0;
}


// precalculated frame for testing
static const u8 frame_test_1[] =  // Frame type 1 - Machine request
{
    0x55, 0x01, 0x01, 0x01, // header
    0x02, // request machine information
    0xf8, 0x61 // crc16
};


// precalculated frame for testing
static const u8 frame_test_2[] = { // Frame type 3 - Instant measure
    0x55, 0x01, 0x03, 0x01, // header
    0xfa, 0xee, // pressure measure (-1298)
    0xfa, 0xee, // flow measure (-1298)
    0x04, 0xd2, // volume measure (1234)
    0x10, 0x2e, // crc16
};


void test_crc_calculation()
{
    size_t length = sizeof(frame_test_1);
    u8 buffer[length];
    memset(buffer, 0, length);
    memcpy(buffer, frame_test_1, length - 2);
    u16 crc = Crc16::calc(0xffff, buffer, length - 2);
    TEST_ASSERT_EQUAL(frame_test_1[length - 2], crc & 0xff);
    TEST_ASSERT_EQUAL(frame_test_1[length - 1], crc >> 8);

    length = sizeof(frame_test_2);
    u8 buffer2[length];
    memset(buffer2, 0, length);
    memcpy(buffer2, frame_test_2, length - 2);
    crc = Crc16::calc(0xffff, buffer2, length - 2);
    TEST_ASSERT_EQUAL(frame_test_2[length - 2], crc & 0xff);
    TEST_ASSERT_EQUAL(frame_test_2[length - 1], crc >> 8);
}


void test_start_of_frame()
{
    reset_serial();
    Hmi01Fsm fsm(dre);
    CircularBuffer& rx = dre.getSmRxBuffer();

    // no started FSM
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_None, fsm.getState());

    // empty frame
    rx.reset();

    // wait for start of frame (0x55)
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // bytes are consumed
    rx.pushByte(0x00);
    TEST_ASSERT_EQUAL(1, rx.getBytesCount());
    fsm.run();
    TEST_ASSERT_EQUAL(0, rx.getBytesCount());
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // skip rubbish
    rx.pushByte(0x00);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // skip buffered rubbish by loop execution
    rx.pushByte(0x54);
    rx.pushByte(0x56);
    rx.pushByte(0x56);
    TEST_ASSERT_EQUAL(3, rx.getBytesCount());
    fsm.run();
    TEST_ASSERT_EQUAL(0, rx.getBytesCount());
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // receive start of frame
    rx.pushByte(0x55);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitForHeader, fsm.getState());
}


void test_valid_frame()
{
    reset_serial();
    CircularBuffer& rx = dre.getSmRxBuffer();

    // just created
    Hmi01Fsm fsm(dre);
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_None, fsm.getState());

    // start consuming
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // receive start of frame
    rx.pushByte(frame_test_1[0]);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitForHeader, fsm.getState());

    // complete the header
    rx.pushBytes(frame_test_1 + 1, 3);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitForPayload, fsm.getState());

    // payload and checksum
    rx.pushBytes(frame_test_1 + 4, 3);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
}


void test_bad_api_error()
{
    reset_serial();
    Hmi01Fsm fsm(dre);
    fsm.run();
    CircularBuffer& rx = dre.getSmRxBuffer();

    RuntimeSystem runtime;
    dre.runtime = &runtime;

    // inject
    u8 buffer[sizeof(frame_test_1)];
    memcpy(buffer, frame_test_1, sizeof(buffer));
    buffer[1] = 0x00; // non valid protocolor version
    rx.pushBytes(buffer, sizeof(buffer));
    fsm.run();
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AckError], dre.getSmTxBuffer().getBytesCount());

    // check error code
    TEST_ASSERT_EQUAL(AckErrorCode_BadApi, ack_error_present());

    // state must be resync
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());

    // resync timeout done
    runtime.setLoopMillis64(runtime.getLoopMillis64() + RESYNC_TIMEOUT_MS);

    // state must be waiting SOF
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
}


void test_invalid_frame()
{
    reset_serial();
    Hmi01Fsm fsm(dre);
    fsm.run();
    CircularBuffer& serial = dre.getSmRxBuffer();

    RuntimeSystem runtime;
    dre.runtime = &runtime;

    // inject
    u8 buffer[sizeof(frame_test_1)];
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    buffer[2] = 0xff; // non valid frame type
    serial.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();

    // check error code
    TEST_ASSERT_EQUAL(AckErrorCode_InvalidFrame, ack_error_present());

    // state must be resync
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());

    // resync timeout done
    runtime.setLoopMillis64(runtime.getLoopMillis64() + RESYNC_TIMEOUT_MS);

    // state must be waiting SOF
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
}


void test_checksum_error()
{
    reset_serial();
    Hmi01Fsm fsm(dre);
    fsm.run();
    CircularBuffer& serial = dre.getSmRxBuffer();

    RuntimeSystem runtime;
    dre.runtime = &runtime;

    // inject
    u8 buffer[sizeof(frame_test_1)];
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    buffer[4]++; // bad checksum
    serial.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();

    // check error code
    TEST_ASSERT_EQUAL(AckErrorCode_CrcError, ack_error_present());

    // state must be resync
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());

    // resync timeout done
    runtime.setLoopMillis64(runtime.getLoopMillis64() + RESYNC_TIMEOUT_MS);

    // state must be waiting SOF
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
}


void test_resync_timeout()
{
    Hmi01Fsm fsm(dre);
    fsm.run();
    CircularBuffer& rx = dre.getSmRxBuffer();

    RuntimeSystem runtime;
    dre.runtime = &runtime;

    // inject frame with crc error to force resync
    u8 buffer[sizeof(frame_test_1)];
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    buffer[sizeof(buffer) - 2]++; // CRC error
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    TEST_ASSERT_EQUAL(AckErrorCode_CrcError, ack_error_present());
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());

    // after resync timout wait for new frames
    runtime.setLoopMillis64(runtime.getLoopMillis64() + RESYNC_TIMEOUT_MS);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // inject invalid frame
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());
    u64 next_timeout = runtime.getLoopMillis64() + RESYNC_TIMEOUT_MS;

    // inject bytes after to extend resync timeout
    runtime.setLoopMillis64(next_timeout - (RESYNC_TIMEOUT_MS / 2));
    rx.pushByte(0x55);
    fsm.run();
    runtime.setLoopMillis64(next_timeout);
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());

    // resync timeout is extended while not silence
    next_timeout += (RESYNC_TIMEOUT_MS / 2) - 1;
    runtime.setLoopMillis64(next_timeout);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_Resync, fsm.getState());
    runtime.setLoopMillis64(next_timeout + 1);
    fsm.run();
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
}


void test_frame_number()
{
    Hmi01Fsm fsm(dre);
    fsm.run();
    CircularBuffer& rx = dre.getSmRxBuffer();
    CircularBuffer& tx = dre.getSmTxBuffer();
    FrameHeader header;

    for (u8 n = 0; n < 5; n++)
    {
        reset_serial();
        rx.pushBytes(frame_test_1, sizeof(frame_test_1));
        fsm.run();
        tx.popBytes((u8*)&header, sizeof(FrameHeader));
        TEST_ASSERT_EQUAL(n, header.FrameNumber);
    }
}


void test_machine_request()
{
    Hmi01Fsm fsm(dre);
    fsm.run();

    CircularBuffer& rx = dre.getSmRxBuffer();
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    u8 buffer[FRAME_MAX_SIZE];
    FrameHeader* header = (FrameHeader*)buffer;
    PayloadRequest* request = (PayloadRequest*)(buffer + FRAME_HEADER_SIZE);

    // FrameType_MachineInfo
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    request->RequestedFrameType = FrameType_MachineInfo;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_MachineInfo, header->FrameType);

    // FrameType_CycleData
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    request->RequestedFrameType = FrameType_CycleData;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_CycleData, header->FrameType);

    // FrameType_MachineParams
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    request->RequestedFrameType = FrameType_MachineParams;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_MachineParams, header->FrameType);

    // FrameType_AlarmParams
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    request->RequestedFrameType = FrameType_AlarmParams;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_AlarmParams, header->FrameType);

    // FrameType_AlarmStatus
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    request->RequestedFrameType = FrameType_AlarmStatus;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_AlarmStatus, header->FrameType);

    // FrameType_InstantMeasure
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    request->RequestedFrameType = FrameType_InstantMeasure;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_AckError, header->FrameType);
}


void test_machine_params()
{
    SettingsManagerFsm sm(dre);
    sm.run(); // loading
    sm.run(); // unlocked

    CircularBuffer& rx = dre.getSmRxBuffer();
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    Hmi01Fsm fsm(dre);
    fsm.run();
    u8 buffer[FRAME_MAX_SIZE];
    FrameHeader* header = (FrameHeader*)buffer;
    PayloadMachineParams* payload = (PayloadMachineParams*)(buffer + FRAME_HEADER_SIZE);

    // current machine params
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    ((PayloadRequest*)(payload))->RequestedFrameType = FrameType_MachineParams;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_MachineParams, header->FrameType);

    // change some value and request
    TEST_ASSERT_EQUAL(MachineParams::VentMode_Stopped, payload->VentMode);
    payload->VentMode = MachineParams::VentMode_Running;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, FRAME_SIZE[FrameType_MachineParams]);
    fsm.run(); // receive request

    // process response
    sm.run(); // settings manager process
    fsm.run(); // send ACK/error
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AckError], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AckError, header->FrameType);
    TEST_ASSERT_EQUAL(AckErrorCode_ACK, ((PayloadAckError*)(payload))->AckErrorCode);

    // current machine params
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    ((PayloadRequest*)(payload))->RequestedFrameType = FrameType_MachineParams;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_MachineParams, header->FrameType);

    // change some value to an OUT OF RANGE and request
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEAK, payload->PeakPressure);
    payload->PeakPressure = ALARM_FACTORY_PEAK_HIGH + 1;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, FRAME_SIZE[FrameType_MachineParams]);
    fsm.run(); // receive request

    // process response
    sm.run(); // settings manager process
    fsm.run(); // send ACK/error
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AckError], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AckError, header->FrameType);
    TEST_ASSERT_EQUAL(MachineParams::ErrorCode_Peak, ((PayloadAckError*)(payload))->AckErrorCode);
}


void test_alarm_limits()
{
    SettingsManagerFsm sm(dre);
    sm.run(); // loading
    sm.run(); // unlocked

    CircularBuffer& rx = dre.getSmRxBuffer();
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    Hmi01Fsm fsm(dre);
    fsm.run();
    u8 buffer[FRAME_MAX_SIZE];
    u8* payload = buffer + FRAME_HEADER_SIZE;
    FrameHeader* header = (FrameHeader*)buffer;

    // current machine params
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    ((PayloadRequest*)payload)->RequestedFrameType = FrameType_AlarmParams;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AlarmParams], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AlarmParams, header->FrameType);
    TEST_ASSERT_NULL(dre.getSmAlarmLimitsRequest());

    // change some value and request
    ((PayloadAlarmLimits*)payload)->PeepPressureHigh = ALARM_FACTORY_PEEP_HIGH - 1;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, FRAME_SIZE[FrameType_AlarmParams]);
    fsm.run(); // receive request
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());

    // process response
    TEST_ASSERT_NOT_NULL(dre.getSmAlarmLimitsRequest());
    sm.run(); // settings manager process
    TEST_ASSERT_NULL(dre.getSmAlarmLimitsRequest());
    fsm.run(); // send ACK/error
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AckError], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AckError, header->FrameType);
    TEST_ASSERT_EQUAL(AckErrorCode_ACK, ((PayloadAckError*)payload)->AckErrorCode);

    // current machine params
    memcpy(buffer, frame_test_1, sizeof(frame_test_1));
    ((PayloadRequest*)payload)->RequestedFrameType = FrameType_AlarmParams;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, sizeof(frame_test_1));
    fsm.run();
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AlarmParams], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AlarmParams, header->FrameType);

    // change some value and request
    TEST_ASSERT_NULL(dre.getSmAlarmLimitsRequest());
    ((PayloadAlarmLimits*)payload)->PeepPressureHigh = ALARM_FACTORY_PEEP_HIGH + 1;
    recalculate_crc(buffer);
    rx.pushBytes(buffer, FRAME_SIZE[FrameType_AlarmParams]);
    fsm.run(); // receive request

    // process response
    TEST_ASSERT_NOT_NULL(dre.getSmAlarmLimitsRequest());
    sm.run(); // settings manager process
    TEST_ASSERT_NULL(dre.getSmAlarmLimitsRequest());
    fsm.run(); // send ACK/error
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AckError], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AckError, header->FrameType);
    TEST_ASSERT_EQUAL(AlarmLimits::ErrorCode_PeepHigh, ((PayloadAckError*)payload)->AckErrorCode);
}


void test_send_measures()
{
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    Hmi01Fsm fsm(dre);
    u8 buffer[FRAME_MAX_SIZE];
    FrameHeader* header = (FrameHeader*)buffer;

    // start
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());

    // new measures
    dre.setEvent(Event_MeasuresNew, true);
    fsm.run();
    dre.setEvent(Event_MeasuresNew, false);

    // instant measure frame is sent
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_InstantMeasure], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_InstantMeasure, header->FrameType);

    // wait new event
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());
}


void test_send_cycle_data()
{
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    VentilationFsm vpc(dre);
    SettingsManagerFsm set(dre);
    Hmi01Fsm fsm(dre);

    u8 buffer[FRAME_MAX_SIZE];
    FrameHeader* header = (FrameHeader*)buffer;

    // start
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());

    // new cycle
    dre.setEvent(Event_ExpirationEnd, true);
    fsm.run();
    dre.setEvent(Event_ExpirationEnd, false);

    // cycle data frame is sent
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_CycleData], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_CycleData, header->FrameType);

    // wait new event
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());
}


void test_send_alarms()
{
    CircularBuffer& tx = dre.getSmTxBuffer();
    CircularBuffer& rx = dre.getSmRxBuffer();
    reset_serial();

    RuntimeSystem runtime;
    dre.runtime = &runtime;

    Hmi01Fsm fsm(dre);
    u8 buffer[FRAME_MAX_SIZE];
    u8* payload = buffer + FRAME_HEADER_SIZE;
    FrameHeader* header = (FrameHeader*)buffer;

    // start
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());

    // alarms changed
    dre.setEvent(Event_AlarmsChanged, true);
    fsm.run();
    dre.setEvent(Event_AlarmsChanged, false);

    // alarms are sent
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AlarmStatus], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AlarmStatus, header->FrameType);

    // alarm frame is resend after ALARM_ACK_SECONDS
    uint64_t ack_timeout = runtime.getLoopMillis64() + (ALARM_ACK_SECONDS * 1000);

    // nothing occurs while not ACK timeout
    runtime.setLoopMillis64(ack_timeout - 1);
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());

    // resend alarms when ACK timeout
    runtime.setLoopMillis64(ack_timeout + 1);
    fsm.run();
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AlarmStatus], tx.popBytes(buffer, sizeof(buffer)));
    TEST_ASSERT_EQUAL(FrameType_AlarmStatus, header->FrameType);

    // send ACK back
    header->FrameType = FrameType_AckError;
    ((PayloadAckError*)payload)->AckErrorCode = AckErrorCode_ACK;
    ((PayloadAckError*)payload)->FrameReference = buffer[0x03];
    recalculate_crc(buffer);
    rx.pushBytes(buffer, FRAME_SIZE[FrameType_AckError]);
    fsm.run();
    TEST_ASSERT_EQUAL(0, rx.getBytesCount());

    // alarms are not sent by ACK timeout
    ack_timeout = runtime.getLoopMillis64() + (ALARM_ACK_SECONDS * 1000);
    runtime.setLoopMillis64(ack_timeout + 1);
    fsm.run();
    TEST_ASSERT_EQUAL(0, tx.getBytesCount());
}


void test_multiple_frames()
{
    u8 buffer[] = 
    { 
        0x55, 0x01, 0x01, 0x01, 0x02, 0xf8, 0x61, // request machine info
        0x55, 0x01, 0x01, 0x02, 0x05, 0xb9, 0x53, // request machine params
        0x55, 0x01, 0x01, 0x03, 0x06, 0xf8, 0xc2, // request alarm settings
        0x55, 0x01, 0x01, 0x04, 0x07, 0x3b, 0x32, // request alarm status
    };

    Hmi01Fsm fsm(dre);
    fsm.run();

    CircularBuffer& rx = dre.getSmRxBuffer();
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    FrameHeader* header = (FrameHeader*)buffer;
    FramePayload* payload = (FramePayload*)(buffer + FRAME_HEADER_SIZE);

    // inject buffer 
    size_t pending_bytes = rx.pushBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(pending_bytes, rx.getBytesCount());

    // process first request
    fsm.run();
    size_t sent_bytes = tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_MachineInfo, header->FrameType);
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_MachineInfo], sent_bytes);
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
    pending_bytes -= FRAME_SIZE[FrameType_Request];
    TEST_ASSERT_EQUAL(pending_bytes, rx.getBytesCount());

    // process second request
    fsm.run();
    sent_bytes = tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_MachineParams, header->FrameType);
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_MachineParams], sent_bytes);
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
    pending_bytes -= FRAME_SIZE[FrameType_Request];
    TEST_ASSERT_EQUAL(pending_bytes, rx.getBytesCount());

    // process third request
    fsm.run();
    sent_bytes = tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_AlarmParams, header->FrameType);
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AlarmParams], sent_bytes);
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
    pending_bytes -= FRAME_SIZE[FrameType_Request];
    TEST_ASSERT_EQUAL(pending_bytes, rx.getBytesCount());

    // process fourth request
    fsm.run();
    sent_bytes = tx.popBytes(buffer, sizeof(buffer));
    TEST_ASSERT_EQUAL(FrameType_AlarmStatus, header->FrameType);
    TEST_ASSERT_EQUAL(FRAME_SIZE[FrameType_AlarmStatus], sent_bytes);
    TEST_ASSERT_EQUAL(Hmi01Fsm::State_WaitFor0x55, fsm.getState());
    pending_bytes -= FRAME_SIZE[FrameType_Request];
    TEST_ASSERT_EQUAL(pending_bytes, rx.getBytesCount());

    // no more pending bytes on buffer
    TEST_ASSERT_EQUAL(0, pending_bytes);
}


void test_buffer_overflow()
{
    Hmi01Fsm fsm(dre);
    fsm.run();

    CircularBuffer& rx = dre.getSmRxBuffer();
    CircularBuffer& tx = dre.getSmTxBuffer();
    reset_serial();

    size_t tx_capacity = tx.getEmptySpace();
    while (tx_capacity-- > 0) tx.pushByte(0x00);
    rx.pushBytes(frame_test_1, sizeof(frame_test_1));

    TEST_ASSERT_FALSE(dre.isEvent(Event_SerialTxOverflow));
    fsm.run();
    TEST_ASSERT_TRUE(dre.isEvent(Event_SerialTxOverflow));
}


void reset_serial()
{
    dre.getSmRxBuffer().reset();
    dre.getSmTxBuffer().reset();
}


AckErrorCode ack_error_present()
{
    CircularBuffer& tx = dre.getSmTxBuffer();
    size_t size = FRAME_SIZE[FrameType_AckError];
    u8 buffer[size];

    // packet was sent
    if (tx.popBytes(buffer, size) == size)
    {
        // error code
        const PayloadAckError* payload = (const PayloadAckError*) (buffer + FRAME_HEADER_SIZE);
        return ((AckErrorCode) payload->AckErrorCode);
    }

    return AckErrorCode_ACK;
}


void recalculate_crc(u8* buffer)
{
    size_t size = FRAME_SIZE[buffer[0x02]] - 2;
    u16 crc = Crc16::calc(0xffff, buffer, size);
    buffer[size++] = crc & 0xff;
    buffer[size] = crc >> 8;
}

