#include <unity.h>
#include <reespirator/Fsm/ReespiratorFsm.hpp>

using namespace reespirator;

static DRE dre;

// tests
void test_initialization();

int main()
{
    // tests
    UNITY_BEGIN();
    RUN_TEST(test_initialization);
    UNITY_END();

    return 0;
}


void test_initialization()
{
    ReespiratorFsm machine(dre);
    TEST_ASSERT_EQUAL(ReespiratorFsm::State_None, machine.getState());

    machine.run();
    TEST_ASSERT_EQUAL(ReespiratorFsm::State_Initializing, machine.getState());

    // wait for subsystems 
    int max_loops = 10;
    while (max_loops-- > 0)
    {
        machine.run();
        if (machine.getState() != ReespiratorFsm::State_Initializing) break;
    }
    TEST_ASSERT_EQUAL(ReespiratorFsm::State_Running, machine.getState());

    // check subsystems
    TEST_ASSERT_GREATER_THAN(SettingsManagerFsm::State_Loading, machine.getSettings().getState());
    TEST_ASSERT_GREATER_THAN(SensorsBuffer::State_None, machine.getSensors().getState());
    TEST_ASSERT_GREATER_THAN(VentilationFsm::State_None, machine.getVentilation().getState());
    TEST_ASSERT_GREATER_THAN(Hmi01Fsm::State_None, machine.getSerialMonitor().getState());
}

