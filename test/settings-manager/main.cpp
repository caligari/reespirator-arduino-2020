
#include <unity.h>
#include <reespirator/Core/Types/AlarmLimits.hpp>
#include <reespirator/Fsm/SettingsManagerFsm.hpp>
#include <reespirator/Fsm/ReespiratorFsm.hpp>

using namespace reespirator;

static DRE dre;

// tests
void test_default_values();
void test_lock_and_unlock();
void test_machine_params_init();
void test_machine_params_range();
void test_alarm_limits_init();
void test_alarm_limits_range();

int main()
{
    ReespiratorFsm mac(dre);

    // tests
    UNITY_BEGIN();
    RUN_TEST(test_default_values);
    RUN_TEST(test_lock_and_unlock);
    RUN_TEST(test_machine_params_init);
    RUN_TEST(test_machine_params_range);
    RUN_TEST(test_alarm_limits_init);
    RUN_TEST(test_alarm_limits_range);
    UNITY_END();

    return 0;
}


void test_default_values()
{
    SettingsManagerFsm fsm(dre);
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VOLUME, dre.currentMachineParams->getVolume());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RPM, dre.currentMachineParams->getRpm());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEAK, dre.currentMachineParams->getPeakPressure());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEEP, dre.currentMachineParams->getPeepPressure());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_FLOW_TRIGGER, dre.currentMachineParams->getTriggerFlow());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RAMP, dre.currentMachineParams->getRamp());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_IE_RATIO_DIV, dre.currentMachineParams->getIeRatioDivisor());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VENT_MODE, dre.currentMachineParams->getVentMode());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VENT_CONTROL, dre.currentMachineParams->getVentControl());
}


void test_lock_and_unlock()
{
    SettingsManagerFsm fsm(dre);
    TEST_ASSERT_EQUAL(SettingsManagerFsm::State_None, fsm.getState());

    RuntimeSystem runtime;
    dre.runtime = &runtime;

    // load settings from persistent storage
    fsm.run();
    TEST_ASSERT_EQUAL(SettingsManagerFsm::State_Loading, fsm.getState());

    // unlocked status
    fsm.run();
    TEST_ASSERT_EQUAL(SettingsManagerFsm::State_Unlocked, fsm.getState());
    u64 start_time = 1000;
    runtime.setLoopMillis64(start_time);

    // request change
    AlarmLimits al = *(dre.currentAlarmLimits);
    dre.setSmAlarmLimitsRequest(&al);

    // locked status
    fsm.run();
    TEST_ASSERT_EQUAL(SettingsManagerFsm::State_Locked, fsm.getState());

    // unlocked after timeout
    runtime.setLoopMillis64(start_time + (LOCK_SOURCE_TIMEOUT_SECONDS * 1000));
    fsm.run();
    TEST_ASSERT_EQUAL(SettingsManagerFsm::State_Unlocked, fsm.getState());
}


void test_machine_params_init()
{
    SettingsManagerFsm fsm(dre);
    fsm.run(); // loading
    fsm.run(); // unlocked

    // factory defaults
    MachineParams mp = *(dre.currentMachineParams);
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VENT_MODE, mp.getVentMode());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VENT_CONTROL, mp.getVentControl());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VOLUME, mp.getVolume());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RPM, mp.getRpm());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEAK, mp.getPeakPressure());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEEP, mp.getPeepPressure());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_FLOW_TRIGGER, mp.getTriggerFlow());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RAMP, mp.getRamp());
    TEST_ASSERT_EQUAL(PARAM_FACTORY_IE_RATIO_DIV, mp.getIeRatioDivisor());
    TEST_ASSERT_EQUAL(0, mp.getRecruitTimer());
    TEST_ASSERT_EQUAL(0, mp.getMutedAlarm());

    // prepare change
    const u16 peep = mp.getPeepPressure();
    mp.setPeepPressure(peep + 1);

    // request settings change from serial monitor
    TEST_ASSERT_NULL(dre.getSmMachineParamsRequest());
    dre.setSmMachineParamsRequest(&mp);
    TEST_ASSERT_NOT_NULL(dre.getSmMachineParamsRequest());

    // accept the request
    fsm.run();
    TEST_ASSERT_NULL(dre.getSmMachineParamsRequest());
    TEST_ASSERT_EQUAL(peep + 1, dre.currentMachineParams->getPeepPressure());
}


void test_machine_params_range()
{
    SettingsManagerFsm fsm(dre);
    fsm.run(); // loading
    fsm.run(); // unlocked

    MachineParams mp;
    const MachineParams* current = dre.currentMachineParams;

    // ventilation mode
    mp = *current;
    mp.setVentMode(MachineParams::VentMode_NUM);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VENT_MODE, current->getVentMode());

    // ventilation control
    mp = *current;
    mp.setVentControl(MachineParams::VentControl_NUM);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VENT_CONTROL, current->getVentControl());

    // volume min
    mp = *current;
    mp.setVolume(PARAM_VOLUME_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VOLUME, current->getVolume());

    // volume max
    mp = *current;
    mp.setVolume(PARAM_VOLUME_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_VOLUME, current->getVolume());

    // rpm min
    mp = *current;
    mp.setRpm(PARAM_RPM_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RPM, current->getRpm());

    // rpm max
    mp = *current;
    mp.setRpm(PARAM_RPM_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RPM, current->getRpm());

    // peak min
    mp = *current;
    mp.setPeakPressure(PARAM_PEAK_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEAK, current->getPeakPressure());

    // peak max
    mp = *current;
    mp.setPeakPressure(PARAM_PEAK_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEAK, current->getPeakPressure());

    // peep min
    mp = *current;
    mp.setPeepPressure(PARAM_PEEP_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEEP, current->getPeepPressure());

    // peep max
    mp = *current;
    mp.setPeepPressure(PARAM_PEEP_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_PEEP, current->getPeepPressure());

    // flow trigger min
    mp = *current;
    mp.setTriggerFlow(PARAM_FLOW_TRIGGER_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_FLOW_TRIGGER, current->getTriggerFlow());

    // flow trigger max
    mp = *current;
    mp.setTriggerFlow(PARAM_FLOW_TRIGGER_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_FLOW_TRIGGER, current->getTriggerFlow());

    // ramp min
    mp = *current;
    mp.setRamp(PARAM_RAMP_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RAMP, current->getRamp());

    // ramp max
    mp = *current;
    mp.setRamp(PARAM_RAMP_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_RAMP, current->getRamp());

    // ie min
    mp = *current;
    mp.setIeRatioDivisor(PARAM_IE_RATIO_DIV_MIN - 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_IE_RATIO_DIV, current->getIeRatioDivisor());

    // ie max
    mp = *current;
    mp.setIeRatioDivisor(PARAM_IE_RATIO_DIV_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(PARAM_FACTORY_IE_RATIO_DIV, current->getIeRatioDivisor());

    // recruit timer
    mp = *current;
    mp.setRecruitTimer(PARAM_RECRUIT_TIMER_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(0, current->getRecruitTimer());

    // muted alarm
    mp = *current;
    mp.setMutedAlarm(PARAM_MUTED_ALARM_MAX + 1);
    dre.setSmMachineParamsRequest(&mp);
    fsm.run();
    TEST_ASSERT_EQUAL(0, current->getMutedAlarm());
}


void test_alarm_limits_init()
{
    SettingsManagerFsm fsm(dre);
    fsm.run(); // loading
    fsm.run(); // unlocked

    // factory defaults
    AlarmLimits al = *(dre.currentAlarmLimits);
    TEST_ASSERT_EQUAL(ALARM_FACTORY_VOLUME_LOW, al.getVolumeLow());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_VOLUME_HIGH, al.getVolumeHigh());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_RPM_LOW, al.getRpmLow());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_RPM_HIGH, al.getRpmHigh());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_PEAK_LOW, al.getPeakPressureLow());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_PEAK_HIGH, al.getPeakPressureHigh());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_PEEP_LOW, al.getPeepPressureLow());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_PEEP_HIGH, al.getPeepPressureHigh());
    TEST_ASSERT_EQUAL(ALARM_FACTORY_BATTERY_LOW, al.getBatteryLow());

    // prepare change
    const u16 rpm = al.getRpmLow();
    al.setRpmLow(rpm + 1);

    // request settings change from serial monitor
    TEST_ASSERT_NULL(dre.getSmAlarmLimitsRequest());
    dre.setSmAlarmLimitsRequest(&al);
    TEST_ASSERT_NOT_NULL(dre.getSmAlarmLimitsRequest());

    // accept the request
    fsm.run();
    TEST_ASSERT_NULL(dre.getSmAlarmLimitsRequest());
    TEST_ASSERT_EQUAL(rpm + 1, dre.currentAlarmLimits->getRpmLow());
}


void test_alarm_limits_range()
{
    SettingsManagerFsm fsm(dre);
    fsm.run(); // loading
    fsm.run(); // unlocked

    const AlarmLimits* current = dre.currentAlarmLimits;
    AlarmLimits al;

    // volume min
    al = *current;
    al.setVolumeLow(ALARM_VOLUME_MIN - 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_VOLUME_MIN, current->getVolumeLow());

    // volume max
    al = *current;
    al.setVolumeHigh(ALARM_VOLUME_MAX + 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_VOLUME_MAX, current->getVolumeHigh());

    // rpm min
    al = *current;
    al.setRpmLow(ALARM_RPM_MIN - 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_RPM_MIN, current->getRpmLow());

    // rpm max
    al = *current;
    al.setRpmHigh(ALARM_RPM_MAX + 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_RPM_MAX, current->getRpmHigh());

    // peak min
    al = *current;
    al.setPeakPressureLow(ALARM_PEAK_MIN - 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_PEAK_MIN, current->getPeakPressureLow());

    // peak max
    al = *current;
    al.setPeakPressureHigh(ALARM_PEAK_MAX + 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_PEAK_MAX, current->getPeakPressureHigh());

    // peep min
    al = *current;
    al.setPeepPressureLow(ALARM_PEEP_MIN - 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_PEEP_MIN, current->getPeepPressureLow());

    // peep max
    al = *current;
    al.setPeepPressureHigh(ALARM_PEEP_MAX + 1);
    dre.setSmAlarmLimitsRequest(&al);
    fsm.run();
    TEST_ASSERT_EQUAL(ALARM_PEEP_MAX, current->getPeepPressureHigh());
}


