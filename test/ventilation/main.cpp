
#include <unity.h>
#include <reespirator/Fsm/VentilationFsm.hpp>
#include <reespirator/Fsm/SettingsManagerFsm.hpp>

using namespace reespirator;

static DRE dre;

// tests
void test_start_stop();
void test_cycle_timing();
void test_ventilation_pc();

int main()
{
    // tests
    UNITY_BEGIN();
    RUN_TEST(test_start_stop);
    RUN_TEST(test_cycle_timing);
    RUN_TEST(test_ventilation_pc);
    UNITY_END();

    return 0;
}


void test_start_stop()
{
    VentilationFsm vent(dre);
    RuntimeSystem rt;
    dre.runtime = &rt;

    // use defaults 
    SettingsManagerFsm sm(dre);
    while (sm.getState() <= SettingsManagerFsm::State_Loading) sm.run();
    MachineParams params = *(dre.currentMachineParams);
    dre.currentMachineParams = &params;

    // ventilation is not running
    TEST_ASSERT_EQUAL(VentilationFsm::State_None, vent.getState());
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_EmptyCycle, vent.getState());
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_EmptyCycle, vent.getState());

    // turn on ventilation
    params.setVentMode(MachineParams::VentMode_Running);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Inspiration, vent.getState());

    // stop ventilation
    params.setVentMode(MachineParams::VentMode_Stopped);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Inspiration, vent.getState());

    // it runs to the end of cycle
    u32 cycle_millis = (1000 * 60 * 10) / params.getRpm();
    rt.setLoopMillis64(rt.getLoopMillis64() + cycle_millis - 100);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Expiration, vent.getState());

    // and finally it stops
    rt.setLoopMillis64(rt.getLoopMillis64() + 200);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_EmptyCycle, vent.getState());

    // turn on ventilation again
    params.setVentMode(MachineParams::VentMode_Running);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Inspiration, vent.getState());

    // pause ventilation
    params.setVentMode(MachineParams::VentMode_Paused);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Inspiration, vent.getState());

    // it runs to the end of cycle
    rt.setLoopMillis64(rt.getLoopMillis64() + cycle_millis - 100);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Expiration, vent.getState());

    // and finally ventilation is paused (peep pressure must be active)
    rt.setLoopMillis64(rt.getLoopMillis64() + 200);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_EmptyCycle, vent.getState());
}


void test_cycle_timing()
{
    RuntimeSystem rt;
    dre.runtime = &rt;
    VentilationFsm vent(dre);

    // use defaults and turn on ventilation
    SettingsManagerFsm sm(dre);
    while (sm.getState() <= SettingsManagerFsm::State_Loading) sm.run();
    MachineParams params = *(dre.currentMachineParams);
    dre.currentMachineParams = &params;
    params.setVentMode(MachineParams::VentMode_Running);

    // timing
    u32 cycle_millis = (1000 * 60 * 10) / dre.currentMachineParams->getRpm();
    u32 inspiration_millis = cycle_millis * 10 / (10 + dre.currentMachineParams->getIeRatioDivisor());
    u32 start = rt.getLoopMillis64();

    // empty cycle
    TEST_ASSERT_EQUAL(VentilationFsm::State_None, vent.getState());
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_EmptyCycle, vent.getState());

    // inspiration
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Inspiration, vent.getState());
    TEST_ASSERT_TRUE(dre.isEvent(Event_InspirationStart));
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Inspiration, vent.getState());
    TEST_ASSERT_FALSE(dre.isEvent(Event_InspirationStart));

    // half cycle
    TEST_ASSERT_FALSE(dre.isEvent(Event_HalfCycle));
    rt.setLoopMillis64(start + inspiration_millis + 100);
    vent.run();
    TEST_ASSERT_TRUE(dre.isEvent(Event_HalfCycle));
    TEST_ASSERT_EQUAL(VentilationFsm::State_Expiration, vent.getState());
    vent.run();
    TEST_ASSERT_FALSE(dre.isEvent(Event_HalfCycle));
    TEST_ASSERT_EQUAL(VentilationFsm::State_Expiration, vent.getState());

    // start again
    rt.setLoopMillis64(start + cycle_millis + 100);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_EmptyCycle, vent.getState());
}


void test_ventilation_pc()
{
    VentilationFsm vent(dre);
    RuntimeSystem rt;
    dre.runtime = &rt;

    // use defaults and turn on ventilation
    SettingsManagerFsm sm(dre);
    while (sm.getState() <= SettingsManagerFsm::State_Loading) sm.run();
    MachineParams params = *(dre.currentMachineParams);
    dre.currentMachineParams = &params;
    params.setVentMode(MachineParams::VentMode_Running);

    // timing
    u32 cycle_millis = (1000 * 60 * 10) / dre.currentMachineParams->getRpm();
    u32 inspiration_millis = cycle_millis * 10 / (10 + dre.currentMachineParams->getIeRatioDivisor());
    u32 peak_millis = (inspiration_millis * params.getRamp()) / 255;
    u64 start = rt.getLoopMillis64();

    // start inspiration with peep
    while (vent.getState() != VentilationFsm::State_Inspiration) vent.run();
    vent.run();
    TEST_ASSERT_EQUAL(params.getPeepPressure(), dre.currentVentilation->getPressure());

    // middle ramp between peep and peak
    rt.setLoopMillis64(start + (peak_millis / 2));
    vent.run();
    TEST_ASSERT_GREATER_THAN(params.getPeepPressure(), dre.currentVentilation->getPressure());
    TEST_ASSERT_LESS_THAN(params.getPeakPressure(), dre.currentVentilation->getPressure());

    // end ramp with peak
    rt.setLoopMillis64(start + peak_millis);
    vent.run();
    TEST_ASSERT_EQUAL(params.getPeakPressure(), dre.currentVentilation->getPressure());

    // end inspiration with peak
    rt.setLoopMillis64(start + inspiration_millis - 100);
    vent.run();
    TEST_ASSERT_EQUAL(params.getPeakPressure(), dre.currentVentilation->getPressure());

    // expiration is peep pressure (driven by valve)
    rt.setLoopMillis64(rt.getLoopMillis64() + 200);
    vent.run();
    TEST_ASSERT_EQUAL(VentilationFsm::State_Expiration, vent.getState());
    vent.run();
    TEST_ASSERT_EQUAL(params.getPeepPressure(), dre.currentVentilation->getPressure());
}

