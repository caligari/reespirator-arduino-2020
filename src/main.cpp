/** main.cpp
 *
 * arduino-2020 main program.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */


//////////////////////////////////////////////////////////////////////
// headers
//////////////////////////////////////////////////////////////////////

#ifdef ARDUINO
#include <Arduino.h>
#endif

#include "reespirator_tasks.hpp"


//////////////////////////////////////////////////////////////////////
// main program
//////////////////////////////////////////////////////////////////////

void setup()
{
    #ifdef ARDUINO
    Serial.begin(115200);
    #endif
}

void loop()
{
    task_ticker.doLoop();
}

// simulate setup-loop pattern
#ifndef ARDUINO
int main()
{
    setup();
    for (;;) loop();
}
#endif


