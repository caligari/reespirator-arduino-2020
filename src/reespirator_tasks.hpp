/** reespirator_tasks.hpp
 *
 * Low-priority tasks for the main loop.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _REESPIRATOR_TASKS_HPP
#define _REESPIRATOR_TASKS_HPP

#include <LoopTicker.hpp>
#include <reespirator/Core/DRE.hpp>
#include <reespirator/SerialPort/SerialPort.hpp>
#include <reespirator/Fsm/ReespiratorFsm.hpp>

using namespace reespirator;


//////////////////////////////////////////////////////////////////////
// Data Runtime Environment
//////////////////////////////////////////////////////////////////////

static DRE dre;


//////////////////////////////////////////////////////////////////////
// serial port for HMI protocol
//////////////////////////////////////////////////////////////////////

#ifdef ARDUINO
static SerialPort serial_hmi(dre.getSmRxBuffer(), dre.getSmTxBuffer(), Serial);
#else
#pragma message ( "SerialPort is needed to use with HMI." )
#endif

//////////////////////////////////////////////////////////////////////
// Ventilation machine FSM
//////////////////////////////////////////////////////////////////////

ReespiratorFsm machine(dre);

//////////////////////////////////////////////////////////////////////
// LoopTicker tasks
//////////////////////////////////////////////////////////////////////

static const LoopTicker::Task loop_tasks[] =
{
    // HAL
    LoopTicker::Task(&serial_hmi, SerialPort::tickUpdate),

    // Reespirator
    LoopTicker::Task(&machine, ReespiratorFsm::tickUpdate),
};

#define LOOP_TASKS_COUNT (sizeof(loop_tasks) / sizeof(loop_tasks[0]))
static LoopTicker task_ticker(loop_tasks, LOOP_TASKS_COUNT);


#endif // _REESPIRATOR_TASKS_HPP
