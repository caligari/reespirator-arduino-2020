# Arduino 2020

Arduino firmware for Reespirator 2020.

[![pipeline status](https://gitlab.com/caligari/reespirator-arduino-2020/badges/dev/pipeline.svg)](https://gitlab.com/caligari/reespirator-arduino-2020/-/jobs)

## Download

    git clone --recursive https://gitlab.com/caligari/reespirator-arduino-2020.git

## Build and upload firmware

    pio run -t upload

## Execute tests

    pio test -e testing


